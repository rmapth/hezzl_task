<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\WorklogRepository")
 */
class Worklog
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @ORM\Column(type="datetime")
     */
    private $finished;

    /**
     * @ORM\Column(type="string", length=1200, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="integer")
     */
    private $user_id;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getFinished(): ?\DateTimeInterface
    {
        return $this->finished;
    }

    public function setFinished(\DateTimeInterface $finished): self
    {
        $this->finished = $finished;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getUserId(): ?int
    {
        return $this->user_id;
    }

    public function setUserId(int $user_id): self
    {
        $this->user_id = $user_id;

        return $this;
    }

    /**
     * validates given data
     *
     * @param array $data
     * @return array
     *
     */
    public function validateNewRecord(array $data){

        /**
         * if field empty
         */
        if(empty($data['created'])){
            return [
                'success'=>false,
                'errorMsg' => 'Field "created" is required'
            ];
        }
        if(empty($data['finished'])){
            return [
                'success'=>false,
                'errorMsg' => 'Field "finished" is required'
            ];
        }
        if(empty($data['user_id'])){
            return [
                'success'=>false,
                'errorMsg' => 'Field "user_id" is required'
            ];
        }


        /**
         * Fields 'created' and 'finished' should be timestamped
         */

        if(!strtotime($data['created'])){
            return [
                'success'=>false,
                'errorMsg' => 'Field "created" has wrong format.'
            ];
        }

        if(!strtotime($data['finished'])){
            return [
                'success'=>false,
                'errorMsg' => 'Field "finished" has wrong format.'
            ];
        }
        if(strtotime($data['created'])>strtotime($data['finished'])){
            return [
                'success'=>false,
                'errorMsg' => 'value "created" can not be greater than value "finished" '
            ];
        }
        /**
         * Otherwise return true
         */

        /**
         * I won't clean data from sql injection for now
         */
        return [
            'success'=>true
        ];
    }

    public function setValues($data,&$self,$mode='create'){
        if(!empty($data['created'])){
            $created = new \DateTime($data['created']);
            $self->setCreated($created);
        }
        if(!empty($data['finished'])){
            $finished = new \DateTime($data['finished']);
            $self->setFinished($finished);
        }

        $desc = empty($data['description'])?'':$data['description'];
        $self->setDescription($desc);
        /**
         * With creating only
         */
        if($mode==='create' && !empty($data['user_id']))
            $self->setUserId($data['user_id']);

        return $self;
    }
}
