<?php
namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Worklog;
use Nelmio\ApiDocBundle\NelmioApiDocBundle;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;

/**
 * Class WorklogController
 * @package App\Controller
 */
class WorklogController extends AbstractController
{
    /**
     * List the records
     * @SWG\Response(
     *     response=200,
     *     description="Returns a list of worklog records",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Worklog::class, groups={"full"}))
     *     )
     * )
     *
     * @SWG\Tag(name="list")
     *
     *@Route("/rest/api/worklog/list/{id}",name="worklog_get_action",methods={"GET"},requirements={"id"="\d+"})
     */
    public function getAction($id=-1)
    {
        /**
         * Abdou 0. In real life all restful apps need token or apikeys to be set, but another time
         */
        /**
         * Abdou 1. Also every method should use try catch but another time
         */

        $em = $this->getDoctrine()->getManager();
        $arWorklogs = []; // result array
        if($id<0){
            $Worklogs = $em->getRepository(Worklog::class)->findAllWorklogs();
            foreach ($Worklogs as $Worklog){
                $arWorklogs[] = [
                    'user_id'       => $Worklog->getUserId(),
                    'created'       => $Worklog->getCreated(),
                    'finished'      => $Worklog->getFinished(),
                    'description'   => $Worklog->getDescription()
                ];
            }
            return $this->json([
                'success' => 'true',
                'items' => json_encode($arWorklogs)
            ]);
        }else{
            $Worklogs = $em->getRepository(Worklog::class)->findById($id);
            if(!empty($Worklogs)){
                $arWorklogs[] = [
                    'user_id'       => $Worklogs->getUserId(),
                    'created'       => $Worklogs->getCreated(),
                    'finished'      => $Worklogs->getFinished(),
                    'description'   => $Worklogs->getDescription()
                ];

            }

            return $this->json([
                'success' => 'true',
                'items' => json_encode($arWorklogs)
            ]);
        }

    }


    /**
     * Create a worklog record
     * @SWG\Response(
     *     response=200,
     *     description="Creates a new record",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Worklog::class, groups={"full"}))
     *     )
     * )
     * @SWG\Parameter(
     *
     *     name="created",
     *     in="query",
     *     type="string",
     *     description="The field showing date the record was created"
     *
     *
     * )
     *
     * @SWG\Tag(name="create")
     *
     * @Route("rest/api/worklog/create}",name="worklog_post_action",methods={"POST"})
     *
     */
    public function postAction($slug = 'empty')
    {
        /**
         *Abdou: 0. Getting request. If empty return error
         *
         * I would make class Erorr with method returnError($code,$message) but another time
         */
        $request = $_POST or json_decode(file_get_contents('php://input'),true);

        if(empty($request))
            return $this->json(json_encode(
                [
                    'success'=>false,
                    'errorMessage' => 'Empty request'
                ]
            ));

        /**
         * Abdou: 1. Getting doctrine entity
         */
        $em = $this->getDoctrine()->getManager();
        $Worklog = new Worklog();
        /**
         * Abdou 2. Validate request;
         */
        $validation = $Worklog->validateNewRecord($request);
        if(!$validation['success']){
            return $this->json(json_encode($validation));
        }else{

            $Worklog->setValues($request,$Worklog);
            /**
             * Abdou 3. Create new record;
             */
            $em->persist($Worklog);
            $em->flush();
            $lastId = $Worklog->getId();

            if(!empty($lastId)){
                return $this->json(json_encode(['success'=>true,'id'=>$lastId],JSON_UNESCAPED_UNICODE));
            }else{
                return $this->json(json_encode(['success'=>false,'errorMsg'=>'Unknown error creating a new record'],JSON_UNESCAPED_UNICODE));
            }
        }
    }

    /**
     * @Route("rest/api/worklog/delete/{id}",name="worklog_delete_action",methods={"DELETE"},requirements={"id"="\d+"})
     */
    public function deleteAction($id=-1)
    {
        /**
         *Abdou: 0. Getting request. If empty return error
         *
         */
        parse_str(file_get_contents('php://input'), $request);

        if(empty($request))
            return $this->json(json_encode(
                [
                    'success'=>false,
                    'errorMessage' => 'Empty request'
                ]
            ));

        if(!empty($request['id'])){
            $id = $request['id'];
        }else if($id<0){
            return $this->json(json_encode(
                [
                    'success'=>false,
                    'errorMessage' => 'Empty id'
                ]
            ));
        }

        /**
         * Abdou: 1. Getting doctrine entity
         */
        $em = $this->getDoctrine()->getManager();
        $Worklog = $em->getRepository(Worklog::class)->findById($id);
        if(empty($Worklog)){
            return $this->json(json_encode(
                [
                    'success'=>false,
                    'errorMessage' => "Unable to find the record with id = $id"
                ]
            ));
        }
        $em->remove($Worklog);
        $em->flush();
        return $this->json(json_encode(
            [
                'success'=>true,
                'message' => "Record with id = $id is deleted"
            ]
        ));

    }
    /**
     * @Route("rest/api/worklog/edit/{id}",name="worklog_patch_action",methods={"PATCH"},requirements={"id"="\d+"})
     */
    public function patchAction($id=-1)
    {
        /**
         *Abdou: 0. Getting request. If empty return error
         *
         */
        parse_str(file_get_contents('php://input'), $request);
        if(empty($request))
            return $this->json(json_encode(
                [
                    'success'=>false,
                    'errorMessage' => 'Empty request'
                ]
            ));
        /**
         * Abdou: 0.1 Select mode
         */
        if(!empty($request['id'])){
            $id = $request['id'];
        }else if($id<0){
            return $this->json(json_encode(
                [
                    'success'=>false,
                    'errorMessage' => 'Empty id'
                ]
            ));
        }

        /**
         * Abdou: 1. Getting doctrine entity
         */
        $em = $this->getDoctrine()->getManager();
        $Worklog = $em->getRepository(Worklog::class)->findById($id);
        if(empty($Worklog)){
            return $this->json(json_encode(
                [
                    'success'=>false,
                    'errorMessage' => "Unable to find the record with id = $id"
                ]
            ));
        }


        $Worklog->setValues($request,$Worklog);
        /**
         * Abdou 3. Create new record;
         */
        $em->persist($Worklog);
        $em->flush();
        $lastId = $Worklog->getId();

        if(!empty($lastId)){
            return $this->json(json_encode(['success'=>true,'id'=>$lastId],JSON_UNESCAPED_UNICODE));
        }else{
            return $this->json(json_encode(['success'=>false,'errorMessage'=>'Unknown error creating a new record'],JSON_UNESCAPED_UNICODE));
        }

    }
}